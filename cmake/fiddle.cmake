find_program(FIDDLEC "fiddlec")

if (FIDDLEC)
  message (STATUS "Found fiddlec at ${FIDDLEC}")
else()
  message (FATAL_ERROR "Could not find fiddlec. Please download and build it at git@git.josher.dev:fiddle.git")
endif()

function(add_fiddle_source header_out fdl_file)
  get_filename_component(dir_path ${fdl_file} DIRECTORY)
  get_filename_component(file_name_without_extension ${fdl_file} NAME_WE)
  set(path_without_extension "${dir_path}/${file_name_without_extension}")
  
  # Define the output path based on the input `.fdl` file's name
  set(output_header "${CMAKE_BINARY_DIR}/generated/${path_without_extension}.h")
  get_filename_component(output_dir ${output_header} DIRECTORY)
  
  # Define the custom command to generate the header
  add_custom_command(
      OUTPUT ${output_header}
      DEPENDS ${fdl_file}
      COMMENT "Fiddle compile ${fdl_file} -> ${output_header}"
      COMMAND ${CMAKE_COMMAND} -E make_directory "${output_dir}"
      COMMAND ${FIDDLEC} -Lc -h ${output_header} -I ${CMAKE_SOURCE_DIR}/fdl/ --intf-dir ${CMAKE_BINARY_DIR}/fdli/ ${CMAKE_SOURCE_DIR}/${fdl_file}
  )
  
  # Make the output header file available as a source file for the target
  set_source_files_properties(${output_header} PROPERTIES GENERATED TRUE)
  set(${header_out} ${output_header} PARENT_SCOPE)
endfunction()

function(fiddle_sources headers_out files)
  set(headers)
  foreach (fdl_file ${files})
    file(RELATIVE_PATH rel_path "${CMAKE_SOURCE_DIR}" "${fdl_file}")
    add_fiddle_source(header_out ${rel_path})
    list(APPEND headers "${header_out}")
  endforeach()
  set(${headers_out} "${headers}" PARENT_SCOPE)
endfunction()
