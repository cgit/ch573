#pragma once

/* Easily turn on and off the system-led. */

#include "ch573/gpio.h"

static inline void enable_sysled()
{
  CH573_GPIO__GPIO_PORT_T_INTF.dir.set(ch573_gpio__gpio_port_b, DIR_OUT, 10);
  CH573_GPIO__GPIO_PORT_T_INTF.pd_drv.set(ch573_gpio__gpio_port_b, 1, 10);
}

static inline void set_sysled(int on)
{
   CH573_GPIO__GPIO_PORT_T_INTF.out.set(
       ch573_gpio__gpio_port_b, on ? ON : OFF, 10);
}
