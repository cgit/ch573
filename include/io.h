#pragma once

#ifndef BAUD_RATE
#define BAUD_RATE 115200
#endif

/** Initializes uart1 for use as stdout. */
void init_uart1_for_stdout(void);
