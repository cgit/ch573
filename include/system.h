#pragma once

#include <stdint.h>

#define SAFE_ACCESS_SIG_REG (*((volatile uint32_t*)0x40001040))

#define SAFE_ACCESS_1 0x57
#define SAFE_ACCESS_2 0xA8

inline static void enter_safe_mode()
{
  SAFE_ACCESS_SIG_REG = SAFE_ACCESS_1;
  SAFE_ACCESS_SIG_REG = SAFE_ACCESS_2;
}
