#pragma once

#include <stdint.h>

typedef void (*systick_cb_t)(void);

void set_systick(uint64_t systick_value);

uint64_t get_systick();

int systick_interrupt();

#define On_SysTick()                                                   \
  static void __local_on_systick__(void);                              \
  __attribute__((                                                      \
      __section__(".systick_callbacks"))) static volatile systick_cb_t \
      __FILE__##__LINE__ = __local_on_systick__;                       \
  static void __local_on_systick__()
