#pragma once

#include <stdint.h>

/* Library for doing fast math functions using just uint8_t. */

extern uint8_t sintable[256];

/* Returns ((sin(n) + 1) / 2) * 255 */
static inline uint8_t byte_sin(uint8_t n)
{
  return sintable[n];
}

uint8_t calc_w(uint8_t n);
