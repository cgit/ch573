#pragma once

#include <stdlib.h>

#include "ch573/spi.h"

#define SPI CH573_SPI__SPI_T_INTF
#define SPI0 ch573_spi__spi0

void enable_spi(void);

void run_spi(void);

void dma_transfer(void* output_buffer, size_t len);

void wait_for_dma();
