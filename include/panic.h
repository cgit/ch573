#pragma once

#include <stdint.h>

/** flashes a code forever. */
void flash_code(uint32_t code);

/** Panics. Call this function when an error is unrrecoverable. */
void panic(uint32_t code);
