# CH573F

## Background

THe CH573F is a relatively new, fairly powerful, little microcontroller. They're
about USD 1.00 a piece on AliExpress, which makes them incredibly cheap for
their abilities, which makes them an ideal replacement for my Arduinos to run my
Christmas lights!

The documentation for the controller is fairly easy to read, though examples are
few and far between and the SDK is mostly in Chinese, but that's what this
project is for. True to form, this project has no dependencies on any 3rd party
SDK or OS. It's completely bare metal from the ground up.

## Specs

The CH573 has some pretty impressive specs for its price point. For the purposes
of this project, it has:

* Royalty-free RISC-V core:
  * 32-bit Qingke RISC-V processor WCH RISC-V3A
  * RV32IMAC instruction set w/ hardware multiplication and division.
  * System frequency from 32KHz up to 60MHz (Though I can only get 6.4MHz, 30MHz
    and 60MHz to work).
* 512K FlashROM
  * 32KB application data
* UART
  * 4 independent UART, with 8-level FIFO
* SPI support with DMA

## Related Work

This project is very similar to:

  * https://git.josher.dev/cgit/stm32l4.git/
  * https://git.josher.dev/cgit/esp32-ws2812b.git/

Together they make up my microcontroller trio for my Christmas lights every
year.

## Setup

To compile this project, one needs a few things.

1. A RISC-V crosscompiler with Picolibc
1. An installed version of "Fiddle", which is my custom
   register-description-language, found at https://git.josher.dev/cgit/fiddle.git
1. ch-flash, or another WCH ISP.
1. Cmake

### RISC-V Cross-compiler with Piclibc

The easiest way to get one of these is to use
[crosstool-ng](https://crosstool-ng.github.io/) with the default
`riscv32-picolibc-elf`

```bash
  $ ct-ng riscv32-picolibc-elf
  $ ct-ng build
  $ ct-ng install
```

### Fiddle

Fiddle is a program used to take language-agnostic register descriptions and
compile them into usable source code. The register descriptions for this project
are provided as fiddle (fdl) files.

The Fiddle compiler is implemented in Haskell using the Haskell stack build
system. Install stack with:

```bash
  $ apt install stack
```

get fiddle:

```bash
  $ git clone https://git.josher.dev/cgit/fiddle.git
  $ cd fiddle
  $ stack install
```

This will compile and install `fiddlec`, which is required to build this
project.

### WCH ISP

There are many open source projects which can flash to the CH573F. I have one in
my git grove, but they're easy to find if another one works better.

```bash
  $ git clone https://git.josher.dev/cgit/ch-flash.git
  $ cd ch-flash
  $ make
```

### CMake

Install cmake through your package manager.

```bash
  $ apt install cmake
```

## Compiling and Flashing

```bash
  $ git clone https://git.josher.dev/ch573.git
  $ cd ch573
  $ cmake -B build
  $ make -C build
  $ /path/to/ch-flash -f build/main.bin
```

To flash, disconnect the ch573 from USB, hold down the BOOT button, then
re-connect it, ch-flash should automatically detect it and flash it. After that,
it all should work.
