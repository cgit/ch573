#include "spi.h"

#include <stdio.h>

#include "ch573/gpio.h"
#include "ch573/spi.h"
#include "sysled.h"

#define MAX_SPI_FIFO 8

#define GPIO_PORT_A ch573_gpio__gpio_port_a
#define GPIO_PORT_B ch573_gpio__gpio_port_b
#define GPIO_PORT CH573_GPIO__GPIO_PORT_T_INTF

#define GPIO_I CH573_GPIO__GPIO_T_INTF
#define GPIO ch573_gpio__gpio

void enable_spi(void)
{
  GPIO_I.pin_alternate.pin_spi0.set(GPIO, ON);

  GPIO_PORT.out.set(GPIO_PORT_A, ON, 12);
  GPIO_PORT.dir.set(GPIO_PORT_A, DIR_OUT, 12);
  GPIO_PORT.dir.set(GPIO_PORT_A, DIR_OUT, 14);
  GPIO_PORT.pd_drv.set(GPIO_PORT_A, 0, 12);
  GPIO_PORT.pd_drv.set(GPIO_PORT_A, 0, 14);

  GPIO_PORT.dir.set(GPIO_PORT_B, DIR_OUT, 12);
  GPIO_PORT.dir.set(GPIO_PORT_B, DIR_OUT, 14);
  GPIO_PORT.pd_drv.set(GPIO_PORT_B, 0, 12);
  GPIO_PORT.pd_drv.set(GPIO_PORT_B, 0, 14);
  GPIO_PORT.out.set(GPIO_PORT_B, OFF, 14);
  GPIO_PORT.out.set(GPIO_PORT_B, ON, 12);

  GPIO_PORT.pu.set(GPIO_PORT_B, ENABLED, 14);

  GPIO_PORT.dir.set(GPIO_PORT_B, DIR_IN, 13);
  GPIO_PORT.dir.set(GPIO_PORT_B, DIR_IN, 15);
  GPIO_PORT.dir.set(GPIO_PORT_A, DIR_IN, 13);
  GPIO_PORT.dir.set(GPIO_PORT_A, DIR_IN, 15);

  SPI.clock_div.set(SPI0, 25);
  SPI.ctrl_mod.all_clear.set(SPI0, 1);
  // SPI.ctrl_mod.set(SPI0, 0xe0);  // Set mosi and sck

  SPI.ctrl_mod.all_clear.set(SPI0, 0);
  SPI.ctrl_mod.pin_enable.set(SPI0, 0x2);  // Set mosi and sck
  SPI.ctrl_cfg.auto_if.set(SPI0, 1);
  SPI.ctrl_cfg.dma_enable.set(SPI0, OFF);
}

// void write_color(uint8_t r, uint8_t g, uint8_t b)
// {
//
//
//   while (!SPI.int_flag.if_fifo_hf.get(SPI0));
//   SPI.fifo.set(SPI0, r);
//   SPI.fifo.set(SPI0, g);
//   SPI.fifo.set(SPI0, b);
// }

void dma_transfer(void* output_buffer, size_t len)
{
  // Clear everything.
  // SPI.ctrl_mod.all_clear.set(SPI0, 1);
  // SPI.ctrl_mod.all_clear.set(SPI0, 0);
  // SPI.ctrl_cfg.dma_enable.set(SPI0, OFF);

  // printf("Start DMA: %p, %zu\n", output_buffer, len);

  SPI.ctrl_mod.fifo_dir.set(SPI0, FIFO_DIR_OUTPUT);
  SPI.dma_beg.set(SPI0, (uint32_t)output_buffer);
  SPI.dma_end.set(SPI0, (uint32_t)(output_buffer + len));

  SPI.total_count.set(SPI0, len);

  // Start DMA.
  SPI.ctrl_cfg.dma_enable.set(SPI0, ON);
}

void wait_for_dma()
{
  while (SPI.total_count.get(SPI0) > 0) {
    set_sysled(1);
  }
  set_sysled(0);
}

void run_spi(void)
{
  GPIO_PORT.out.set(GPIO_PORT_A, ON, 12);
  while (1) {
    SPI.total_count.set(SPI0, 1024);
    // GPIO_PORT_A->clr |= 1 << 12;

    while (SPI.fifo_count.get(SPI0) == 8);

    SPI.fifo.set(SPI0, 0xaa);
    SPI.total_count.set(SPI0, 1024);
  }
}
