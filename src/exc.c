#include <stdio.h>

#include "isr_vector.h"
#include "panic.h"

#include "ch573/gpio.h"

#define GPIO_PORT_A ch573_gpio__gpio_port_a
#define GPIO_PORT CH573_GPIO__GPIO_PORT_T_INTF

void delay();

IRQ(exc)
{
  uint32_t mcause, mepc, mtval, *sp;

  asm volatile("csrr %0, mcause" : "=r"(mcause));
  asm volatile("csrr %0, mepc" : "=r"(mepc));
  asm volatile("csrr %0, mtval" : "=r"(mtval));

  printf("Hardware Exception Caught:\n");
  printf("  mcause: 0x%08x\n", mcause);
  printf("    mepc: 0x%08x\n", mepc);
  printf("   mtval: 0x%08x\n", mtval);

  panic(mcause);
}

IRQ(nmi)
{
  uint32_t mcause, mepc, mtval, *sp;

  asm volatile("csrr %0, mcause" : "=r"(mcause));
  asm volatile("csrr %0, mepc" : "=r"(mepc));
  asm volatile("csrr %0, mtval" : "=r"(mtval));

  printf("Unhandled NMI Caught:\n");
  printf("  mcause: 0x%08x\n", mcause);
  printf("    mepc: 0x%08x\n", mepc);
  printf("   mtval: 0x%08x\n", mtval);

  panic(mcause);
}
