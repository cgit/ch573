#include "systick.h"

#include <stdio.h>

#include "ch573/systick.h"
#include "isr_vector.h"

#define SYSTICK_I CH573_SYSTICK__SYSTICK_T_INTF
#define SYSTICK ch573_systick__systick

void set_systick(uint64_t systick)
{
  SYSTICK_I.reload.set(SYSTICK, systick);
  SYSTICK_I.cfg.st_reload.set(SYSTICK, 1);
  SYSTICK_I.cfg.interrupt_enable.set(SYSTICK, 1);

  SYSTICK_I.cfg.enabled.set(SYSTICK, ENABLED);
}

uint64_t get_systick()
{
  return ((uint64_t)SYSTICK_I.count_high.get(SYSTICK) << 32) |
         SYSTICK_I.count_low.get(SYSTICK);
}

int systick_interrupt()
{
  return SYSTICK_I.counter_interrupt_flag.get(SYSTICK);
}

extern systick_cb_t SYSTICK_LISTENERS_START;
extern systick_cb_t SYSTICK_LISTENERS_END;
IRQ(systick)
{
  systick_cb_t* cur = &SYSTICK_LISTENERS_START;
  while (cur != &SYSTICK_LISTENERS_END) {
    (*cur)();
    ++cur;
  }
  SYSTICK_I.counter_interrupt_flag.set(SYSTICK, 0);
}
