.section .isr_vector
.global isr_vector
isr_vector:
.align 4
.word 0
  j irq_on_reset
  j irq_on_nmi
  j irq_on_exc

.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0
.word 0

  j irq_on_systick

.word 0

  j irq_on_swi

.word 0

  j irq_on_tmr0
  j irq_on_gpio_a
  j irq_on_gpio_b
  j irq_on_spi0
  j irq_on_blel
  j irq_on_bleb
  j irq_on_usb

.word 0

  j irq_on_tmr1
  j irq_on_tmr2
  j irq_on_uart0
  j irq_on_uart1
  j irq_on_rtc
  j irq_on_adc

.word 0

  j irq_on_pwmx
  j irq_on_tmr3
  j irq_on_uart2
  j irq_on_uart3
  j irq_on_wdog_bat
